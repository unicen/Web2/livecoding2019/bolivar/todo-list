"use strict"

let app = new Vue({
    el: "#vue-template-tareas",
    data: {
        title: "Lista renderizada mediante CSR utilizando Vue.js ;)",
        loading: false,
        tareas: [] // es como el $this->smarty->assign("tareas", $tareas);
    }
});


// btn refresh
document.querySelector("#btn-refresh").addEventListener('click', getTareas);

function getTareas() {
    // inicia la carga
    app.loading = true;

    fetch("api/tareas")
    .then(response => response.json())
    .then(tareas => {
        app.tareas  = tareas;
        app.loading = false;
    })
    .catch(error => console.log(error));
}


getTareas();
