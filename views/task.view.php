<?php
    require_once('libs/Smarty.class.php');
    require_once('helpers/auth.helper.php');

class TaskView {

    private $smarty;

    public function __construct() {
        $authHelper = new AuthHelper();
        $userName = $authHelper->getLoggedUserName();
        
        $this->smarty = new Smarty();
        $this->smarty->assign('basehref', BASE_URL);
        $this->smarty->assign('userName', $userName);
    }

    public function showTasks($tareas) {
        $this->smarty->assign('titulo', 'Lista de Tareas');
        $this->smarty->assign('tareas', $tareas);

        $this->smarty->display('templates/taskList.tpl');
    }

    public function showError($msgError) {
        echo "<h1>ERROR!</h1>";
        echo "<h2>{$msgError}</h2>";
    }

    /**
     * Construye el html que permite visualizar el detalle de una tarea determinada.
     */
    public function showTask($tarea) {
        $this->smarty->assign('titulo', 'Detalle de Tarea');
        $this->smarty->assign('tarea', $tarea);

        $this->smarty->display('templates/taskDetail.tpl');
    }
}