{include 'templates/header.tpl'}
<div class="container">
    <h1>Lista de tareas</h1>

    <form action="nueva" method="POST" enctype="multipart/form-data">

        <div class="row">
            <div class="col-9">
                <div class="form-group">
                    <label>Título</label>
                    <input name="titulo" type="text" class="form-control">
                </div>
            </div>
            <div class="col">
                <div class="form-group">
                    <label>Prioridad</label>
                    <select name="prioridad" class="form-control">
                    <option value="1">1 (baja)</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5 (alta)</option>
                    </select>
                </div>
            </div>
        </div>
    
        <div class="form-group">
            <label>Descripcion</label>
            <textarea name="descripcion" class="form-control" rows="3"></textarea>
        </div>
        <div class="form-group">
            <input type="file" name="input_name" id="imageToUpload">
        </div>
        <button type="submit" class="btn btn-primary">Guardar</button>

        </form>

    <ul class="list-group mt-4">
    {foreach $tareas as $tarea }
        <li class="tarea list-group-item
        {if $tarea->finalizada == 1 }
            finalizada
        {/if}
        "> {$tarea->titulo} | {$tarea->descripcion|truncate:15:"..."} 
        <small><a href="tarea/{$tarea->id_tarea}">VER</a></small>
        {if !$tarea->finalizada}
            <small><a href="finalizar/{$tarea->id_tarea}">LISTO</a></small>
        {/if}
        <small><a href="eliminar/{$tarea->id_tarea}">ELIMINAR</a></li></small>
    {/foreach}
    </ul>
</div>
{include 'templates/footer.tpl'}