{literal}
<section id="vue-template-tareas">

        <div class="card">
            <div class="card-header d-flex justify-content-between align-items-center">
                <h4 class="mb-0">Tareas</h4>
                <button id="btn-refresh" type="button" class="btn btn-primary btn-sm">Refresh</button>
            </div>

            <div v-if="loading" class="card-body">
                Cargando...
            </div>

            <ul v-if="!loading" class="list-group list-group-flush">
                <a v-for="tarea in tareas" v-bind:href="'tarea/' + tarea.id_tarea" class="list-group-item list-group-item-action" v-bind:class="{ 'disabled finalizada': tarea.finalizada == '1' }"> 
                    {{ tarea.titulo }} 
                </a>
            </ul>

            <div class="card-footer text-muted">
                {{ title }}
            </div>
        </div>

       
        
</section>
{/literal}
