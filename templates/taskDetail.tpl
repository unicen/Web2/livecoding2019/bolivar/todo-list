{include 'templates/header.tpl'}
<div class="container">
    <div class="row">
      
        <div class="col-md-9">
        <h1>{$tarea->titulo} ({$tarea->prioridad})</h1>
        <p>{$tarea->descripcion|wordwrap:50:"<br/>\n"}</p>
        {if isset($tarea->imagen)}
            <img src="{$tarea->imagen}"/>
        {/if}       
      </div>
      
      <div class="col-md-3">
        {include 'vue/sidebarTask.tpl'}
      </div>  
    </div>
   

</div>

<script src="js/tareas.js"></script>
{include 'templates/footer.tpl'}